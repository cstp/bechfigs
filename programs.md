#Programs:
* VIM (Text editor)
* i3 (Window Manager)
* zsh (Shell)
* rxvt-unicode (Terminal)
* google-chrome
* pavucontrol (Sound control)
* pa-applet (Sound applet)

#Setup:
1. run home.sh
2. xrdb -merge .Xresources
3. chsh -s /usr/bin/zsh (ubuntu)
4. Install https://github.com/fernandotcl/pa-applet
