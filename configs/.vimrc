execute pathogen#infect()
if has("syntax")
    syntax on
endif
if has("autocmd")
  filetype plugin indent on
endif
set showcmd
set showmatch
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab
set number 
filetype plugin on
set omnifunc=syntaxcomplete#Complete
inoremap jj <ESC>
set noundofile

map tn :tabnext <RETURN>
map tb :tabprevious <RETURN>

map <Left> <Nop>
map <Right> <Nop>
map <Up> <Nop>
map <Down> <Nop>

inoremap <Left> <Nop>
inoremap <Right> <Nop>
inoremap <Up> <Nop>
inoremap <Down> <Nop>

inoremap <expr> <C-Space> pumvisible() \|\| &omnifunc == '' ?
            \ "\<lt>C-n>" :
            \ "\<lt>C-x>\<lt>C-o><c-r>=pumvisible() ?" .
            \ "\"\\<lt>c-n>\\<lt>c-p>\\<lt>c-n>\" :" .
            \ "\" \\<lt>bs>\\<lt>C-n>\"\<CR>"
imap <C-@> <C-Space>
"Panes
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
